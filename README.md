# Icinga Monitoring Network

This demonstration repository builds a testing / development network consisting
of a Debian Icinga master node, several agent nodes (which run Icinga 2) and
several remote nodes (which are only monitored externally) these hosts are all
defined in the [hosts.yml](hosts.yml) file and are organised into three groups,
`icinga_master_nodes` ( you can only have one master node),
`icinga_agent_nodes` and `icinga_remote_nodes`.

All `icinga_agent_nodes` and `icinga_remote_nodes` have the
`icinga_master_node` set as a parent node.

For each host an `icinga_check_commands` dictionary is used to specify [the
check
commands](https://icinga.com/docs/icinga-2/snapshot/doc/10-icinga-template-library/#plugin-check-commands-for-monitoring-plugins)
from the [Icinga Template
Library](https://icinga.com/docs/icinga2/latest/doc/10-icinga-template-library/),
for example using remote checks:

```yml
        webarch.email:
          icinga_parent_node: "{{ icinga_master_node }}"
          icinga_check_commands:
            ping:
            mailcow:
              command: http
              args:
                http_ssl: 1
                http_string: "Webarchitects Co-operative Email"
                http_uri: /
            sogo:
              command: http
              args:
                http_ssl: 1
                http_string: "SOGo Groupware"
                http_uri: /SOGo/
```

Or tests run by Icinga on the node:

```yml
        dns5.webarch.info:
          icinga_parent_node: icinga-satellite1.webarchitects.org.uk
          icinga_check_commands:
            apt:
            disk:
            dns:
            dns_webarch_net:
              command: dns
              args:
                dns_authoritative: 1
                dns_server: dns5.webarch.info
                dns_lookup: www.webarch.net
            load:
              args:
                load_percpu: 1
            ping:
            ssh:
            swap:
            users:
              args:
                users_wgreater: 2
                users_cgreater: 4
```

The Icinga 2 and Icinga Web 2 packages used are the Debian packages from
[icinga.com](https://icinga.com/) and these are installed by the [Icinga2
role](https://git.coop/webarch/icinga/) and the [Icinga Web 2
role](https://git.coop/webarch/icingaweb/), the external roles are listed in
the [requirements.yml](requirements.yml) file.

If you want to build your own Icinga monitoring testing / development network
you could try copying this repo and editing the [hosts.yml](hosts.yml) file to
suit, with reference to the [outstanding
issues](https://git.coop/webarch/icinga-network/-/issues), there is a [thread
on the Icinga Discourse forum about these
roles](https://community.icinga.com/t/4455), please use release tags in your
`requirements.yml` file and  [contact
Webarchitects](https://www.webarchitects.coop/contact) if you need help.

## TODO

* Add a lot of test to ensure that there are not two master nodes and that all other nodes have parents etc
* Add the ability to specify a list of users and servers they have access to via Icinga Web 2

## Instructions

```bash
ansible-galaxy install -r requirements.yml --force &&
  ansible-playbook icinga.yml
```

## Command Line

The configuration can be checked with `icinga2 daemon -C` and restarted with `service icinga2 restart`.

The `icingacli` command can be used on the master server, it can be used to
enable and disable Icinga Web 2 modules: 

```bash
icingacli module list
MODULE         VERSION   STATE     DESCRIPTION
setup          2.7.3     enabled   Setup module

icingacli module enable monitoring
icingacli module disable setup

icingacli module list
MODULE         VERSION   STATE     DESCRIPTION
monitoring     2.7.3     enabled   Icinga monitoring module
```

To list hosts:

```bash
icingacli monitoring list hosts
   UP    dns0.webarchitects.co.uk: PING OK - Packet loss = 0%, RTA = 0.47 ms
   UP    dns1.webarchitects.co.uk: PING OK - Packet loss = 0%, RTA = 0.38 ms
   UP    dns2.webarch.info: PING OK - Packet loss = 0%, RTA = 13.93 ms
   UP    dns3.webarch.info: PING OK - Packet loss = 0%, RTA = 81.17 ms
   UP    dns5.webarch.info: PING OK - Packet loss = 0%, RTA = 0.47 ms
   UP    git.coop: PING OK - Packet loss = 0%, RTA = 1.12 ms
   UP    icinga-master.webarchitects.org.uk: PING OK - Packet loss = 0%, RTA = 0.10 ms
   UP    icinga-satellite1.webarchitects.org.uk: PING OK - Packet loss = 0%, RTA = 2.39 ms
   UP    icinga-satellite2.webarchitects.org.uk: PING OK - Packet loss = 0%, RTA = 0.98 ms
   UP    members.webarchitects.coop: PING OK - Packet loss = 0%, RTA = 2.78 ms
   UP    nextcloud.webarch.org.uk: PING OK - Packet loss = 0%, RTA = 1.07 ms
   UP    webarch.email: PING OK - Packet loss = 0%, RTA = 2.91 ms
   UP    wsh.webarchitects.org.uk: PING OK - Packet loss = 0%, RTA = 1.60 ms
   UP    www.webarchitects.coop: PING OK - Packet loss = 0%, RTA = 0.84 ms
```

To list services:

```bash
icingacli monitoring list services 

   UP    dns0.webarchitects.co.uk: PING OK - Packet loss = 0%, RTA = 0.47 ms
   OK    ├─ ping4 (For 1d 6h)
   OK    └─ check-ping (Since 17:42)

   UP    dns1.webarchitects.co.uk: PING OK - Packet loss = 0%, RTA = 1.03 ms
   OK    ├─ ping4 (For 1d 6h)
   OK    └─ check-ping (Since 17:42)

   UP    dns2.webarch.info: PING OK - Packet loss = 0%, RTA = 13.93 ms
   OK    ├─ ping4 (Since 17:09)
   OK    └─ check-ping (For 59m 57s)

   UP    dns3.webarch.info: PING OK - Packet loss = 0%, RTA = 81.17 ms
   OK    ├─ ping4 (Since 20:13)
   OK    └─ check-ping (For 56m 40s)

   UP    dns5.webarch.info: PING OK - Packet loss = 0%, RTA = 0.47 ms
   OK    ├─ ping4 (For 1d 6h)
   OK    ├─ ssh (For 1d 6h)
   OK    ├─ apt (For 1d 1h)
   OK    ├─ check-dns_webarch_net (Since 17:58)
   OK    ├─ check-ping (Since 17:58)
   OK    ├─ check-ssh (Since 17:58)
   OK    ├─ check-load (Since 17:58)
   OK    ├─ check-disk (Since 17:58)
   OK    ├─ check-dns (Since 17:58)
   OK    ├─ check-swap (Since 17:58)
   OK    ├─ check-users (Since 17:58)
   OK    └─ check-apt (Since 17:58)

   UP    git.coop: PING OK - Packet loss = 0%, RTA = 1.12 ms
   OK    ├─ ping4 (For 1d 6h)
   OK    ├─ check-ping (Since 17:42)
   OK    └─ check-http (Since 17:58)

   UP    icinga-master.webarchitects.org.uk: PING OK - Packet loss = 0%, RTA = 0.10 ms
   OK    ├─ icinga (Since May 14)
   OK    ├─ ping6 (Since May 14)
   OK    ├─ swap (Since May 14)
   OK    ├─ ping4 (Since May 14)
   OK    ├─ disk / (Since May 14)
   OK    ├─ http (Since 20:18)
   OK    ├─ load (Since May 14)
   OK    ├─ procs (Since May 14)
   OK    ├─ users (Since May 14)
   OK    ├─ disk (Since May 14)
   OK    └─ ssh (Since May 14)

   UP    icinga-satellite1.webarchitects.org.uk: PING OK - Packet loss = 0%, RTA = 2.39 ms
   OK    ├─ ping4 (For 1d 6h)
   OK    ├─ apt (For 1d 6h)
   OK    └─ ssh (For 1d 6h)

   UP    icinga-satellite2.webarchitects.org.uk: PING OK - Packet loss = 0%, RTA = 1.06 ms
   OK    ├─ ssh (For 1d 6h)
   OK    ├─ apt (Since 00:58)
   OK    └─ ping4 (For 1d 6h)

   UP    members.webarchitects.coop: PING OK - Packet loss = 0%, RTA = 1.81 ms
   OK    ├─ ping4 (For 1d 6h)
   OK    └─ check-ping (Since 17:42)

   UP    nextcloud.webarch.org.uk: PING OK - Packet loss = 0%, RTA = 1.24 ms
   OK    ├─ check-ping (Since 17:58)
   OK    ├─ check-load (Since 17:58)
   OK    ├─ check-swap (Since 17:58)
   OK    ├─ ssh (Since 17:58)
   OK    ├─ apt (Since 17:57)
   OK    ├─ check-ssh (Since 17:58)
   OK    ├─ check-users (Since 17:58)
   OK    ├─ check-dns (Since 17:58)
   OK    ├─ check-disk (Since 17:57)
   OK    ├─ ping4 (Since 17:58)
   OK    ├─ check-apt (Since 17:58)
   OK    └─ check-vhost_default (Since 17:58)

   UP    webarch.email: PING OK - Packet loss = 0%, RTA = 2.91 ms
   OK    ├─ ping4 (For 1d 6h)
   OK    └─ check-ping (Since 17:41)

   UP    wsh.webarchitects.org.uk: PING OK - Packet loss = 0%, RTA = 1.60 ms
   OK    ├─ ssh (For 1d 6h)
   OK    ├─ ping4 (For 1d 6h)
   OK    ├─ apt (Since 11:09)
   OK    ├─ check-vhost_default (Since 14:15)
   OK    ├─ check-ping (Since 17:42)
   OK    ├─ check-apt (Since 17:41)
   OK    ├─ check-swap (Since 17:42)
   OK    ├─ check-ssh (Since 17:42)
   OK    ├─ check-vhost_matomo (Since 17:41)
   OK    ├─ check-users (Since 17:41)
   OK    ├─ check-load (Since 17:42)
   OK    ├─ check-dns (Since 17:42)
   OK    └─ check-disk (Since 17:41)

   UP    www.webarchitects.coop: PING OK - Packet loss = 0%, RTA = 0.84 ms
   OK    ├─ ping4 (For 1d 6h)
   OK    └─ check-ping (Since 17:41)
```

## Notes

The install of Icinga2 packages on the master node is done following the
instructions from the [install
document](https://icinga.com/docs/icinga2/latest/doc/02-installation/). 

Example Apache configuration  can be generated using the `icingacli setup
config webserver apache` command, the output from this has been incorporated
into the [Apache master node
template](https://git.coop/webarch/icingaweb/-/blob/master/templates/apache.conf.j2)
in the [Icinga Web 2 role](https://git.coop/webarch/icingaweb/).

The Icinga API is [setup using the command
line](https://icinga.com/docs/icinga2/latest/doc/12-icinga2-api/#setting-up-the-api).

Two Icinga [API
users](https://icinga.com/docs/icinga2/latest/doc/12-icinga2-api/#authentication)
are created, `root` and `icingaweb2` these are listed in the [icinga role
defaults
file](https://git.coop/webarch/icinga/-/blob/master/defaults/main.yml), the
passwords for these users are written to dot files in `/root` on the master
node in addition to the user files in `/etc/icinga2/conf.d/api-users` and for
`icingaweb2` to the
[/etc/icingaweb2/modules/monitoring/commandtransports.ini](https://icinga.com/docs/icingaweb2/latest/modules/monitoring/doc/05-Command-Transports/#configuration-in-icinga-web-2)
file.

To change the password for a user simply delete the `/root` dot file and on the
next run a new one will be generated. Additional API users can be defined but
it is important that the two default ones are not removed so copy the existing
list and add to it.

The Icinga Web 2 configuration was originally implemented [following these
tasks](https://icinga.com/docs/icingaweb2/latest/doc/20-Advanced-Topics/#automating-the-installation-of-icinga-web-2)
however that didn't result in a website that displayed any data, so an option
to stop the install and allow the wizard to run was added, if
`icingaweb_setup_wizard` is set to try before the install you can use the
wizard.

Since then the working config that the wizard generate has been added to the
repo so the automatic install is fully working.

The web login is written to a `/root` dot file on the master node.

## Links

* The [Icinga2 Ansible Roles](https://github.com/Icinga/ansible-playbooks) and the WIP [Icinga 2 Role for Ansible](https://github.com/Icinga/ansible-icinga2) from [Icinga](https://community.icinga.com/t/1419/2).
* The [Full install as a service](https://www.fiaas.co/) explained in this [Icinga2 Ansible collection](https://www.stroobant.be/Icinga2-ansible-collection) blog post and [the repo for the playbooks and roles](https://github.com/fiaasco/icinga2).
* The [Icinga2 ansible role](https://gitlab.com/bodsch/ansible-icinga2) and the [Icinga community discussion thread](https://community.icinga.com/t/3848). 
